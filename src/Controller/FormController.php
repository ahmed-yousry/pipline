<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\PostType;
use App\Entity\Post;

use App\Repository\PostRepository;



class FormController extends AbstractController
{
    /**
     * @Route("/form", name="form")
     */
    public function index(Request $request)
    {

      $post = new Post();

      $form =  $this->createForm(PostType::class,$post,[
        'action'=> $this->generateUrl('form')
      ]);


       $form->handleRequest($request);

       if ($form->isSubmitted() && $form->isValid()) {
         $file = $request->files->get('post')['my_file'];
         $uploads_directory =  $this->getParameter('uploads_directory');
          $filename = md5(uniqid()).'.'.$file->guessExtension();
          $file->move(
            $uploads_directory,
            $filename
          );
         $Doctrine = $this->getDoctrine()->getManager();

         /*insert into  data base */
         $Doctrine->persist($post);
         $Doctrine->flush();
         /*End*/


       }


        return $this->render('form/index.html.twig', [
            'post_form' => $form->createView()
        ]);
    }


    /**
     * @Route("/showpost/{id}", name="show_post")
     */
    public function  showpost(Request $request,Post $post)
    {



      return $this->render('article/showpost.html.twig', [
          'post' => $post
      ]);
    }



    /**
     * @Route("/repo/{title}", name="repo")
     */
    public function  repo(Request $request,Post $post,PostRepository $PostRepository)
    {

        $post2 = $PostRepository->yousry_SQL_Function();
       // $post2 = $PostRepository->findBy([
       //   'title'=>'title',
       //   'description'=>'de'
       // ]);

      return $this->render('article/showpost.html.twig', [
          'post' => $post,
          'PostRepository'=>$post2
      ]);




    }

}

<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Post;

class ArticleController extends AbstractController
{
    /**
     * @Route("/",name="home")
     */
    public function homepage()
    {
        return new Response('welcome   ahmed  in toto');
    }
    /**
     * @Route("/news/{slug}")
     */
    public function show($slug)
    {
        $comments = [
            'i am ahmed and this is  commands ',
            'i am ahmed and this is  commands ',
            'i am ahmed and this is  commands ',
            'i am ahmed and this is  commands '
        ];

        // return new Response(\sprintf('this is new page '.$slug));
        return $this->render('article/show.html.twig', [
            'title' => \ucwords(\str_replace('-', ' ', $slug)),
            'comments' => $comments
        ]);
    }

    /**
     * @Route("/addpost")
     */
    public function addpost(Request $request)
    {
        /* Form  desion code  */
        $form = $this->createFormBuilder()
            ->add('title')
            ->add('discrbtion')

            ->getForm();
        /*end*/
        $post = new Post();
        $post->setTitle("this is  set title ");
        $post->setDescription("this is setDescription");

        $Doctrine = $this->getDoctrine()->getManager();

        /*insert into  data base */
        $Doctrine->persist($post);
        $Doctrine->flush();
        /*End*/

        /*show data  from  database */
        $retrivePost = $Doctrine->getRepository(Post::class)->findOneBy([
            'id' => 2
        ]);
        var_dump($retrivePost);
        /*End*/


        /*delete into  data base */
        // $Doctrine->remove($post);
        // $Doctrine->flush();
        /*End*/




        return $this->render('article/addpost.html.twig', [
            'user_form' => $form->createView()
        ]);
    }
}

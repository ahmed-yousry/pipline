<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    // /**
    //  * @return Post[] Returns an array of Post objects
    //  */

    public function findByExampleField($value)
    {

        return $this->createQueryBuilder('post')
            ->andWhere('post.title = :val')
            ->setParameter('val', $value)
            ->orderBy('post.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    public function yousry_SQL_Function()
    {
      // $qb =  $this->createQueryBuilder('p');
      // $qb->select('p.title','p.description');
      //
      // dump($qb->getQuery()->getResult());
      // return $qb->getQuery()->getResult();

      $connection =  $this->getEntityManager()->getConnection();
      $sql="SELECT * FROM `post` ORDER BY `title` ASC ";
      $statment = $connection->prepare($sql);
      $statment->execute(['username'=>'admin']);
      return  $statment->fetchAll(); 
    }


    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
